// PrzybyloMateusz3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

float dX = 1.0f;
float dY = 1.0f;
float xstep = 0.0f;
float ystep = 0.0f;

float viewWidth;
float viewHeight;

void Display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(xstep, ystep, 0.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(-58.0f, -25.0f);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(15.5f, -45.5f);

	glVertex2f(-58.0f, -25.0f);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(-30.0f, -45.0f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(-30.0f, -45.0f);
	glVertex2f(15.5f, -45.5f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(45.5f, -15.5f);
	glVertex2f(15.5f, -45.5f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(45.5f, -15.5f);
	glVertex2f(50.5f, 25.5f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(50.5f, 25.5f);
	glVertex2f(25.0f, 60.0f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(25.0f, 60.0f);
	glVertex2f(-30.0f, 60.0f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(-30.0f, 60.0f);
	glVertex2f(-60.0f, 40.0f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(-60.0f, 40.0f);
	glVertex2f(-70.0f, 0.0f);
	glEnd();



	if (xstep > viewWidth)
		dX = -dX;
	if (xstep < -viewWidth)
		dX = -dX;

	if (ystep > viewHeight)
		dY = -dY;
	if (ystep < -viewHeight)
		dY = -dY;

	xstep += dX;
	ystep += dY;

	glutSwapBuffers();
	glutPostRedisplay();
}

void onResize(int w, int h)
{
	// Zabezpieczenie przed dzieleniem przez zero  
	if (h == 0)
		h = 1;

	float aspect = (float)w / (float)h;
	// Ustawienie wielko�ci widoku na r�wn� wielko�ci okna   
	glViewport(0, 0, w, h);
	// Ustalenie uk�adu wsp�rz�dnych  
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Wyznaczenie przestrzeni ograniczaj�cej (lewy, prawy, dolny, g�rny, bliski, odleg�y) 

	if (w <= h)
	{
		glOrtho(-100.0f, 100.0, -100.0f / aspect, 100.0 / aspect, 1.0f, -1.0f);
		viewWidth = 100.0f;
		viewHeight = 100.0f / aspect;
	}
	else
	{
		glOrtho(-100.0 * aspect, 100.0 * aspect, -100.0f, 100.0, 1.0f, -1.0f);
		viewWidth = 100.0f * aspect;
		viewHeight = 100.0f;
	}

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void SetupRenderingContext()
{
	glClearColor(0.5f, 0.35f, 0.05f, 1.0f);
}

///////////////////////////////////////////////////////////
// G��wny punkt wejcia programu
int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutCreateWindow("M�j pierwszy program w GLUT");
	SetupRenderingContext();
	glutReshapeFunc(onResize);
	glutDisplayFunc(Display);
	glutMainLoop();
	return 0;
}


